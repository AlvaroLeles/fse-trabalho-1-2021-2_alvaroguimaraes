# FSE-Trabalho 1 - 2021-2_AlvaroGuimaraes



## Nome
Trabalho 1 de Fundamentos de Sistemas Embarcados (2021/2)

## Descrição
Projeto que simula um sistema de controle de temperatura de um forno para soldagem de placas de circuito impresso.

Essa simulação de controle de temperatura ocorre utilizando dois atuadores: um resistor de potência de 15 Watts e uma ventoinha que puxa o ar externo.

Projeto referente ao Trabalho 1 da matéria "Fundamentos de Sistemas Embarcados" da Universidade de Brasília.

O enunciado deste trabalho pode ser encontrado em https://gitlab.com/fse_fga/trabalhos-2021_2/trabalho-1-2021-2

## Processo de compilação
Para utilizar o sistema, primeiramente deve-se copiar a pasta src e os arquivos Makefile, curva_reflow.csv e log.csv em uma pasta e esta pasta para uma Raspberry Pi.

A partir disso, basta, na Raspberry, estar na pasta copiada e utilizar o comando
```
make
```

Se a compilação ocorrer corretamente, deve-se executar o programa com o comando
```
bin/controlTemp /dev/i2c-1
```

## Uso
Ao executar o programa, o sistema inicialmente pedirá os valores das constantes Kp, Ki e Kd, que serão utilizados enquanto o sistema estiver rodando.

Logo após, o sistema perguntará como a temperatura de referência será definida, caso a opção "Potenciômetro" seja escolhida, todos os seguintes comandos deverão ser fornecidos através do site que contém o dashboard referente a raspberry onde o programa está rodando.

O sistema demonstrará que está se comunicando com a UART o tempo todo, e registrará os comandos que foram requisitados a partir da dashboard.

Caso a opção "Teclado" seja escolhida, deve-se informar a temperatura de referência e o sistema irá esquentar ou esfriar até a temperatura requerida. Ao chegar num valor aproximado da temperatura requerida, o sistema irá encerrar.

Caso a opção "Curva de referência" seja escolhida, o sistema funcionará a partir do arquivo "curva_reflow.csv", seguindo as temperaturas lá especificadas nos tempos informados, também no arquivo.

Para encerrar o sistema, o atalho Ctrl + C deve ser pressionado.

## Experimentos

### Potenciômetro

#### Temperaturas

![Gráfico de temperaturas usando o potênciometro como referência](/assets/TemperaturasPotenciometro.png)

#### Ativação atuadores

![Gráfico de ativação dos atuadores usando o potênciometro como referência](/assets/AcionamentoPotenciometro.png)

### Curva de Reflow

#### Temperaturas

![Gráfico de temperaturas usando a curva de reflow como referência](/assets/TemperaturasCurva.png)

#### Ativação atuadores

![Gráfico de ativação dos atuadores usando a curva de reflow como referência](/assets/AcionamentoCurva.png)

## Problemas conhecidos
Não é possível alterar o modo de referência durante o programa, caso queira executar outro modo de referência, deve-se parar o programa e começá-lo novamente.

Os botões liga e desliga da Dashboard são respondidos apenas no modo "Potênciometro", nos outros modos, o sistema liga em dado momento no início do processo e desliga em seu encerramento.