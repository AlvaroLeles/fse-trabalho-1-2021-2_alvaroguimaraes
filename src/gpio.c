#include "gpio.h"

#define RESIS_PIN 4
#define VENT_PIN 5


void ligaResistor(int valorResistor) {
    pinMode(RESIS_PIN, OUTPUT);
    softPwmCreate(RESIS_PIN, 0, 100);
    softPwmWrite(RESIS_PIN, valorResistor);
}


void ligaVentoinha(int valorVentoinha) {
    pinMode(VENT_PIN, OUTPUT);
    softPwmCreate(VENT_PIN, 0, 100);
    softPwmWrite(VENT_PIN, valorVentoinha);
}

void desligaResistor() {
    pinMode(RESIS_PIN, OUTPUT);
    softPwmCreate(RESIS_PIN, 0, 100);
    softPwmWrite(RESIS_PIN, 0);
}

void desligaVentoinha() {
    pinMode(VENT_PIN, OUTPUT);
    softPwmCreate(VENT_PIN, 0, 100);
    softPwmWrite(VENT_PIN, 0);
}

void controlePWM(int sinalControle) {
    if (sinalControle > 0)
    {
        desligaVentoinha();
        ligaResistor(sinalControle);
    }
    else
    {
        desligaResistor();
        sinalControle = sinalControle * -1;
        if (sinalControle < 40) {
            ligaVentoinha(40);
        }
        else {
            ligaVentoinha(sinalControle);
        }
    }
}