#include <softPwm.h>
#include <wiringPi.h>

#ifndef GPIO_H_
#define GPIO_H_

void ligaResistor(int valorResistor);
void ligaVentoinha(int valorVentoinha);
void desligaResistor();
void desligaVentoinha();
void controlePWM(int sinalControle);

#endif /* GPIO_H_ */