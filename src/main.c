//#ifdef __KERNEL__
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
//#endif

#include <string.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>         //Used for UART
#include <fcntl.h>          //Used for UART
#include <termios.h>        //Used for UART
#include <signal.h>
#include "uart.h"
#include "crc16.h"

#include "meuBme.h"
#include "i2clcd.h"
#include "pid.h"
#include "gpio.h"

// BME
struct identifier
{
    /* Variable to hold device address */
    uint8_t dev_addr;

    /* Variable that contains file descriptor */
    int8_t fd;
};

// LCD
int fd;  // seen by all subroutines

int uartFilestream;

/// Meus métodos
void handle_sigint(int sig);
int getEnderecoESP32();
int enviaEstadoSistema(int uartFilestream, int estado);
int enviaModoReferencia(int uartFilestream, int modoReferencia);
int leCmdUsuar(int uartFilestream);
float getTemperaturaInterna(int uartFilestream);
float getTemperaturaExterna(struct bme280_dev *dev);
float getTemperauraReferenciaPot(int uartFilestream);
void mostraTemperaturas(int uartFilestream, struct bme280_dev *dev, int modoRef);
void enviaSinalControle(int uartFilestream, int sinalControle);
void enviaSinalReferencia(int uartFilestream, float sinalReferencia);
const char* getfield(char* linha, int num);
void desligaSistema();
void insereLog(int uartFilestream, struct bme280_dev *dev, int modoRef);

int main(int argc, const char * argv[]) {
    signal(SIGINT, handle_sigint);

    uartFilestream = uart_init();

    unsigned char tx_buffer[20];
    unsigned char *p_tx_buffer;
    p_tx_buffer = &tx_buffer[0];

    struct bme280_dev dev;

    struct identifier id;

    /* Variable to define the result */
    int8_t rslt = BME280_OK;

    if (argc < 2)
    {
        fprintf(stderr, "Missing argument for i2c bus.\n");
        exit(1);
    }

    if ((id.fd = open(argv[1], O_RDWR)) < 0)
    {
        fprintf(stderr, "Failed to open the i2c bus %s\n", argv[1]);
        exit(1);
    }

//#ifdef __KERNEL__
    id.dev_addr = BME280_I2C_ADDR_PRIM;

    if (ioctl(id.fd, I2C_SLAVE, id.dev_addr) < 0)
    {
        fprintf(stderr, "Failed to acquire bus access and/or talk to slave.\n");
        exit(1);
    }

//#endif

    /* Make sure to select BME280_I2C_ADDR_PRIM or BME280_I2C_ADDR_SEC as needed */

    dev.intf = BME280_I2C_INTF;
    dev.read = user_i2c_read;
    dev.write = user_i2c_write;
    dev.delay_us = user_delay_us;

    /* Update interface pointer with the structure that contains both device address and file descriptor */
    dev.intf_ptr = &id;

    /* Initialize the bme280 */
    rslt = bme280_init(&dev);
    if (rslt != BME280_OK)
    {
        fprintf(stderr, "Failed to initialize the device (code %+d).\n", rslt);
        exit(1);
    }

    //Inicializa o display
    if (wiringPiSetup() == -1)
        exit(1);

    fd = wiringPiI2CSetup(I2C_ADDR);
    
    lcd_init(); // setup LCD
    ClrLcd();

    printf("Informe o valor das constantes Kp, Ki e Kd\n");
    printf("Kp:");
    float kp;
    scanf("%f", &kp);
    printf("Ki:");
    float ki;
    scanf("%f", &ki);
    printf("Kd:");
    float kd;
    scanf("%f", &kd);

    pid_configura_constantes(kp, ki, kd);

    int reEnvEst = enviaEstadoSistema(uartFilestream, 0); // Garante que o sistema está desligado antes de qualquer coisa

    if (reEnvEst == 1)
    {
        printf("Como a temperatura de referência será definida?\n");
        printf("1 - Potenciômetro\n");
        printf("2 - Teclado\n");
        printf("3 - Curva de temperatura\n");
        printf("0 - Sair\n");
    }
    else
    {
        printf("Problema na inicialização do sistema\n");
        close(uartFilestream);
        return 0;
    }

    int modoReferencia;
    scanf("%d", &modoReferencia);
    if (modoReferencia == 1) // Potenciômetro
    {
        reEnvEst = enviaEstadoSistema(uartFilestream, 1); //Liga o sistema
        int resEnvModoRef = enviaModoReferencia(uartFilestream, 0); //Diz que os dados serão lidos do potenciômetro
        if (reEnvEst == 1)
        {
            printf("Sistema ligado\n");
            if (resEnvModoRef == 1)
            {
                while (1)
                {
                    int cmd = leCmdUsuar(uartFilestream);
                    
                    if (cmd > 0)
                    {
                        if (cmd == 1) // Liga o Sistema
                            enviaEstadoSistema(uartFilestream, 1);
                        else if (cmd == 2) // Desliga o Sistema e os atuadores
                        {
                            enviaEstadoSistema(uartFilestream, 0);
                            desligaResistor();
                            desligaVentoinha();
                        }
                        else if (cmd == 3) //Leitura Potenciometro
                        {
                            enviaModoReferencia(uartFilestream, 0);
                            float tempPot = getTemperauraReferenciaPot(uartFilestream);
                            pid_atualiza_referencia(tempPot);
                            float tempInt = getTemperaturaInterna(uartFilestream);
                            int sinalControle = pid_controle(tempInt);
                            enviaSinalControle(uartFilestream, sinalControle);
                            controlePWM(sinalControle);
                            insereLog(uartFilestream, &dev, modoReferencia);
                        }
                        // else if (cmd == 4) //Leitura Curva
                        //     enviaModoReferencia(uartFilestream, 1);
                    }
                    else
                    {
                        mostraTemperaturas(uartFilestream, &dev, modoReferencia);
                        if (modoReferencia == 1)
                        {
                            float tempPot = getTemperauraReferenciaPot(uartFilestream);
                            pid_atualiza_referencia(tempPot);
                            float tempInt = getTemperaturaInterna(uartFilestream);
                            int sinalControle = pid_controle(tempInt);
                            enviaSinalControle(uartFilestream, sinalControle);
                            controlePWM(sinalControle);
                            insereLog(uartFilestream, &dev, modoReferencia);
                        }
                    }
                    sleep(1);
                }
            }
            else
            {
                printf("Problema ao enviar modo de referência\n");
            }
        }
        else
        {
            printf("Problema na inicialização\n");
            close(uartFilestream);
            return 0;
        }
    }
    else if (modoReferencia == 2) // Teclado
    {
        reEnvEst = enviaEstadoSistema(uartFilestream, 1); //Liga o sistema
        if (reEnvEst == 1)
        {
            printf("Sistema ligado\n");
            ClrLcd();
            lcdLoc(LINE1);
            typeln("Leitura");
            lcdLoc(LINE2);
            typeln("teclado");
            printf("Digite a temperatura de referência: ");
            float tempRef;
            scanf("%f", &tempRef);

            enviaSinalReferencia(uartFilestream, tempRef);
            pid_atualiza_referencia(tempRef);

            float tempInt = getTemperaturaInterna(uartFilestream);
            while ((int)tempInt != (int)tempRef)
            {
                tempInt = getTemperaturaInterna(uartFilestream);
                int sinalControle = pid_controle(tempInt);
                enviaSinalControle(uartFilestream, sinalControle);
                controlePWM(sinalControle);
                insereLog(uartFilestream, &dev, modoReferencia);

                mostraTemperaturas(uartFilestream, &dev, modoReferencia);
                sleep(1);
            }
            printf("Temperatura aproximada alcançada\n");
            enviaEstadoSistema(uartFilestream, 0);
            desligaResistor();
            desligaVentoinha();
        }
        else
        {
            printf("Problema na inicialização\n");
            desligaSistema();
        }
    }
    else if (modoReferencia == 3) // Curva de Referência
    {
        reEnvEst = enviaEstadoSistema(uartFilestream, 1); //Liga o sistema
        int resEnvModoRef = enviaModoReferencia(uartFilestream, 1); //Diz que os dados serão lidos do arquivo de curva
        if (reEnvEst == 1)
        {
            printf("Sistema ligado\n");
            if (resEnvModoRef == 1)
            {
                FILE* stream = fopen("curva_reflow.csv", "r");

                if (!stream)
                {
                    printf("Não foi possível abrir o arquivo\n");
                    desligaSistema();
                }

                char linha[1024];
                fgets(linha, 1024, stream); // Descarta a linha dos títulos

                char* tmp;
                float tempVez;

                for (int i = 0; i <= 300; i++)
                {
                    if (i % 30 == 0)
                    {
                        fgets(linha, 1024, stream);
                        tmp = strdup(linha);
                        tempVez = (float)atof(getfield(tmp, 2));
                        enviaSinalReferencia(uartFilestream, tempVez);
                        pid_atualiza_referencia(tempVez);
                    }
                    
                    float tempInt = getTemperaturaInterna(uartFilestream);
                    int sinalControle = pid_controle(tempInt);
                    enviaSinalControle(uartFilestream, sinalControle);
                    controlePWM(sinalControle);
                    insereLog(uartFilestream, &dev, modoReferencia);

                    free(tmp);
                    mostraTemperaturas(uartFilestream, &dev, modoReferencia);
                }
            }
            else
            {
                printf("Problema ao enviar modo de referência\n");
                desligaSistema();
            }
        }
        else
        {
            printf("Problema na inicialização\n");
            desligaSistema();
        }
    }
    
    else if (modoReferencia == 0)
    {
        printf("Saindo do programa \n");
        close(uartFilestream);
        return 0;
    }
    else
    {
        printf("Comando inválido, encerrando sistema\n");
        close(uartFilestream);
        return 0;
    }

    desligaSistema();
    return 0;
}

/// Meus métodos
void handle_sigint(int sig)
{
    desligaSistema();
}

int getEnderecoESP32()
{
    return 0x01;
}

int enviaEstadoSistema(int uartFilestream, int estado)
{
    unsigned char tx_buffer[20];
    unsigned char *p_tx_buffer;
    p_tx_buffer = &tx_buffer[0];

    *p_tx_buffer++ = getEnderecoESP32(); // Endereço da ESP32
    *p_tx_buffer++ = 0x16; // Código
    *p_tx_buffer++ = 0xD3; // Sub-código
    // Matrícula
    *p_tx_buffer++ = 6;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 1;

    *p_tx_buffer++ = estado;
    
    //crc
    short crc = calcula_CRC(&tx_buffer[0], 8);
    unsigned char *crcByte = malloc(2);
    crcByte[0] = crc & 0xff;
    crcByte[1] = (crc >> 8) & 0xff;
    *p_tx_buffer++ = crcByte[0];
    *p_tx_buffer++ = crcByte[1];

    // Envia para a UART
    int resultado = escreveUART(uartFilestream, &tx_buffer[0], p_tx_buffer);
    if (resultado == 1) // Envio deu certo
    {
        sleep(1);
        // Lê o que foi mandado como resposta
        unsigned char *rx_buffer = malloc(256);
        int rx_length = leUART(uartFilestream, &rx_buffer[0]);
        if (rx_buffer && rx_length > 0) //Leitura deu certo
        {
            unsigned char *tx_buffer_resp = malloc(rx_length - 2);
            unsigned char *crcResp = malloc(2);
            // Pega o conteúdo da resposta
            for (int i = 0; i < rx_length - 2; i++)
            {
                tx_buffer_resp[i] = rx_buffer[i];
            }
            // Pega o crc da resposta
            crcResp[0] = rx_buffer[rx_length - 2];
            crcResp[1] = rx_buffer[rx_length - 1];

            // Confere crc da resposta
            short meuCrcResp = calcula_CRC(&tx_buffer_resp[0], rx_length - 2);
            unsigned char *meuCrcByteResp = malloc(2);
            meuCrcByteResp[0] = meuCrcResp & 0xff;
            meuCrcByteResp[1] = (meuCrcResp >> 8) & 0xff;

            if (meuCrcByteResp[0] == crcResp[0] &&
                meuCrcByteResp[1] == crcResp[1]) // crc válido
                {
                    // Pega apenas o dado da resposta
                    unsigned char *intByte = malloc(4);
                    intByte[0] = tx_buffer_resp[3];
                    intByte[1] = tx_buffer_resp[4];
                    intByte[2] = tx_buffer_resp[5];
                    intByte[3] = tx_buffer_resp[6];
                    
                    int retornoEstado = *(int *)&intByte[0];
                    if (retornoEstado == estado)
                    {
                        if (estado == 0)
                        {
                            ClrLcd();
                            lcdLoc(LINE1);
                            typeln("Sistema");
                            lcdLoc(LINE2);
                            typeln("desligado");
                        }
                        else if (estado == 1)
                        {
                            ClrLcd();
                            lcdLoc(LINE1);
                            typeln("Sistema");
                            lcdLoc(LINE2);
                            typeln("ligado");
                        }
                        else
                        {
                            ClrLcd();
                            lcdLoc(LINE1);
                            typeln("Erro ;(");
                            return 0;
                        }
                        sleep(1);
                        return 1;
                    }
                }
            else
                return 0;
        }
        return 0;
    }
    else
        return 0;
}

int enviaModoReferencia(int uartFilestream, int modoReferencia)
{
    unsigned char tx_buffer[20];
    unsigned char *p_tx_buffer;
    p_tx_buffer = &tx_buffer[0];

    *p_tx_buffer++ = getEnderecoESP32(); // Endereço da ESP32
    *p_tx_buffer++ = 0x16; // Código
    *p_tx_buffer++ = 0xD4; // Sub-código
    // Matrícula
    *p_tx_buffer++ = 6;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 1;

    *p_tx_buffer++ = modoReferencia;
    
    //crc
    short crc = calcula_CRC(&tx_buffer[0], 8);
    unsigned char *crcByte = malloc(2);
    crcByte[0] = crc & 0xff;
    crcByte[1] = (crc >> 8) & 0xff;
    *p_tx_buffer++ = crcByte[0];
    *p_tx_buffer++ = crcByte[1];

    // Envia para a UART
    int resultado = escreveUART(uartFilestream, &tx_buffer[0], p_tx_buffer);
    if (resultado == 1) // Envio deu certo
    {
        sleep(1);
        // Lê o que foi mandado como resposta
        unsigned char *rx_buffer = malloc(256);
        int rx_length = leUART(uartFilestream, &rx_buffer[0]);
        if (rx_buffer) //Leitura deu certo
        {
            unsigned char *tx_buffer_resp = malloc(rx_length - 2);
            unsigned char *crcResp = malloc(2);
            // Pega o conteúdo da resposta
            for (int i = 0; i < rx_length - 2; i++)
            {
                tx_buffer_resp[i] = rx_buffer[i];
            }
            // Pega o crc da resposta
            crcResp[0] = rx_buffer[rx_length - 2];
            crcResp[1] = rx_buffer[rx_length - 1];

            // Confere crc da resposta
            short meuCrcResp = calcula_CRC(&tx_buffer_resp[0], rx_length - 2);
            unsigned char *meuCrcByteResp = malloc(2);
            meuCrcByteResp[0] = meuCrcResp & 0xff;
            meuCrcByteResp[1] = (meuCrcResp >> 8) & 0xff;

            if (meuCrcByteResp[0] == crcResp[0] &&
                meuCrcByteResp[1] == crcResp[1]) // crc válido
                {
                    // Pega apenas o dado da resposta
                    unsigned char *intByte = malloc(4);
                    intByte[0] = tx_buffer_resp[3];
                    intByte[1] = tx_buffer_resp[4];
                    intByte[2] = tx_buffer_resp[5];
                    intByte[3] = tx_buffer_resp[6];
                    
                    int retornoModoControle = *(int *)&intByte[0];
                    if (retornoModoControle == modoReferencia)
                    {
                        if (modoReferencia == 0)
                        {
                            ClrLcd();
                            lcdLoc(LINE1);
                            typeln("Leitura");
                            lcdLoc(LINE2);
                            typeln("potenciometro");
                        }
                        else if (modoReferencia == 1)
                        {
                            ClrLcd();
                            lcdLoc(LINE1);
                            typeln("Leitura");
                            lcdLoc(LINE2);
                            typeln("curva");
                        }
                        else
                        {
                            ClrLcd();
                            lcdLoc(LINE1);
                            typeln("Erro ;(");
                            return 0;
                        }
                        sleep(1);
                        return 1;
                    }
                }
            else
                return 0;
        }
        return 0;
    }
    else
        return 0;
}

int leCmdUsuar(int uartFilestream)
{
    unsigned char tx_buffer[20];
    unsigned char *p_tx_buffer;
    p_tx_buffer = &tx_buffer[0];

    *p_tx_buffer++ = getEnderecoESP32(); // Endereço da ESP32
    *p_tx_buffer++ = 0x23; // Código
    *p_tx_buffer++ = 0xC3; // Sub-código
    // Matrícula
    *p_tx_buffer++ = 6;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 1;
    
    //crc
    short crc = calcula_CRC(&tx_buffer[0], 7);
    unsigned char *crcByte = malloc(2);
    crcByte[0] = crc & 0xff;
    crcByte[1] = (crc >> 8) & 0xff;
    *p_tx_buffer++ = crcByte[0];
    *p_tx_buffer++ = crcByte[1];

    // Envia para a UART
    int resultado = escreveUART(uartFilestream, &tx_buffer[0], p_tx_buffer);
    if (resultado == 1) // Envio deu certo
    {
        sleep(1);
        // Lê o que foi mandado como resposta
        unsigned char *rx_buffer = malloc(256);
        int rx_length = leUART(uartFilestream, &rx_buffer[0]);
        if (rx_buffer) //Leitura deu certo
        {
            unsigned char *tx_buffer_resp = malloc(rx_length - 2);
            unsigned char *crcResp = malloc(2);
            // Pega o conteúdo da resposta
            for (int i = 0; i < rx_length - 2; i++)
            {
                tx_buffer_resp[i] = rx_buffer[i];
            }
            // Pega o crc da resposta
            crcResp[0] = rx_buffer[rx_length - 2];
            crcResp[1] = rx_buffer[rx_length - 1];

            // Confere crc da resposta
            short meuCrcResp = calcula_CRC(&tx_buffer_resp[0], rx_length - 2);
            unsigned char *meuCrcByteResp = malloc(2);
            meuCrcByteResp[0] = meuCrcResp & 0xff;
            meuCrcByteResp[1] = (meuCrcResp >> 8) & 0xff;

            if (meuCrcByteResp[0] == crcResp[0] &&
                meuCrcByteResp[1] == crcResp[1]) // crc válido
                {
                    // Pega apenas o dado da resposta
                    unsigned char *intByte = malloc(4);
                    intByte[0] = tx_buffer_resp[3];
                    intByte[1] = tx_buffer_resp[4];
                    intByte[2] = tx_buffer_resp[5];
                    intByte[3] = tx_buffer_resp[6];
                    
                    int comandoUsuario = *(int *)&intByte[0];
                    printf("cmdUsr: %d\n", comandoUsuario);
                    return comandoUsuario;
                }
            else
                return 0;
        }
        return 0;
    }
    else
        return 0;
}

float getTemperaturaInterna(int uartFilestream)
{
    unsigned char tx_buffer[20];
    unsigned char *p_tx_buffer;
    p_tx_buffer = &tx_buffer[0];

    *p_tx_buffer++ = getEnderecoESP32(); // Endereço da ESP32
    *p_tx_buffer++ = 0x23; // Código
    *p_tx_buffer++ = 0xC1; // Sub-código
    // Matrícula
    *p_tx_buffer++ = 6;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 1;
    
    // crc
    short crc = calcula_CRC(&tx_buffer[0], 7);
    unsigned char *crcByte = malloc(2);
    crcByte[0] = crc & 0xff;
    crcByte[1] = (crc >> 8) & 0xff;
    *p_tx_buffer++ = crcByte[0];
    *p_tx_buffer++ = crcByte[1];

    // Envia para a UART
    int resultado = escreveUART(uartFilestream, &tx_buffer[0], p_tx_buffer);
    if (resultado == 1) // Envio deu certo
    {
        sleep(1);
        // Lê o que foi mandado como resposta
        unsigned char *rx_buffer = malloc(256);
        int rx_length = leUART(uartFilestream, &rx_buffer[0]);
        if (rx_buffer) //Leitura deu certo
        {
            unsigned char *tx_buffer_resp = malloc(rx_length - 2);
            unsigned char *crcResp = malloc(2);
            // Pega o conteúdo da resposta
            for (int i = 0; i < rx_length - 2; i++)
            {
                tx_buffer_resp[i] = rx_buffer[i];
            }
            // Pega o crc da resposta
            crcResp[0] = rx_buffer[rx_length - 2];
            crcResp[1] = rx_buffer[rx_length - 1];

            // Confere crc da resposta
            short meuCrcResp = calcula_CRC(&tx_buffer_resp[0], rx_length - 2);
            unsigned char *meuCrcByteResp = malloc(2);
            meuCrcByteResp[0] = meuCrcResp & 0xff;
            meuCrcByteResp[1] = (meuCrcResp >> 8) & 0xff;

            if (meuCrcByteResp[0] == crcResp[0] &&
                meuCrcByteResp[1] == crcResp[1]) // crc válido
                {
                    // Pega apenas o dado da resposta
                    unsigned char *floatByte = malloc(4);
                    floatByte[0] = tx_buffer_resp[3];
                    floatByte[1] = tx_buffer_resp[4];
                    floatByte[2] = tx_buffer_resp[5];
                    floatByte[3] = tx_buffer_resp[6];
                    
                    float temperatura = *(float *)&floatByte[0];
                    return temperatura;
                }
            else
                return -1;
        }
        return -1;
    }
    else
        return -1;
}

float getTemperaturaExterna(struct bme280_dev *dev)
{
    /* Variable to define the result */
    int8_t rslt = BME280_OK;

    /* Variable to define the selecting sensors */
    uint8_t settings_sel = 0;

    /* Variable to store minimum wait time between consecutive measurement in force mode */
    uint32_t req_delay;

    /* Structure to get the pressure, temperature and humidity values */
    struct bme280_data comp_data;

    /* Recommended mode of operation: Indoor navigation */
    dev->settings.osr_h = BME280_OVERSAMPLING_1X;
    dev->settings.osr_p = BME280_OVERSAMPLING_16X;
    dev->settings.osr_t = BME280_OVERSAMPLING_2X;
    dev->settings.filter = BME280_FILTER_COEFF_16;

    settings_sel = BME280_OSR_PRESS_SEL | BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_FILTER_SEL;

    /* Set the sensor settings */
    rslt = bme280_set_sensor_settings(settings_sel, dev);
    if (rslt != BME280_OK)
    {
        fprintf(stderr, "Failed to set sensor settings (code %+d).", rslt);

        return -1;
    }

    /*Calculate the minimum delay required between consecutive measurement based upon the sensor enabled
     *  and the oversampling configuration. */
    req_delay = bme280_cal_meas_delay(&dev->settings);

    /* Continuously stream sensor data */
    /* Set the sensor to forced mode */
    rslt = bme280_set_sensor_mode(BME280_FORCED_MODE, dev);
    if (rslt != BME280_OK)
    {
        fprintf(stderr, "Failed to set sensor mode (code %+d).", rslt);
        return -1;
    }

    /* Wait for the measurement to complete and print data */
    dev->delay_us(req_delay, dev->intf_ptr);
    rslt = bme280_get_sensor_data(BME280_ALL, &comp_data, dev);
    if (rslt != BME280_OK)
    {
        fprintf(stderr, "Failed to get sensor data (code %+d).", rslt);
        return -1;
    }

    return comp_data.temperature;
}

float getTemperauraReferenciaPot(int uartFilestream)
{
    unsigned char tx_buffer[20];
    unsigned char *p_tx_buffer;
    p_tx_buffer = &tx_buffer[0];

    *p_tx_buffer++ = getEnderecoESP32(); // Endereço da ESP32
    *p_tx_buffer++ = 0x23; // Código
    *p_tx_buffer++ = 0xC2; // Sub-código
    // Matrícula
    *p_tx_buffer++ = 6;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 1;
    
    //crc
    short crc = calcula_CRC(&tx_buffer[0], 7);
    unsigned char *crcByte = malloc(2);
    crcByte[0] = crc & 0xff;
    crcByte[1] = (crc >> 8) & 0xff;
    *p_tx_buffer++ = crcByte[0];
    *p_tx_buffer++ = crcByte[1];

    // Envia para a UART
    int resultado = escreveUART(uartFilestream, &tx_buffer[0], p_tx_buffer);
    if (resultado == 1) // Envio deu certo
    {
        sleep(1);
        // Lê o que foi mandado como resposta
        unsigned char *rx_buffer = malloc(256);
        int rx_length = leUART(uartFilestream, &rx_buffer[0]);
        if (rx_buffer) //Leitura deu certo
        {
            unsigned char *tx_buffer_resp = malloc(rx_length - 2);
            unsigned char *crcResp = malloc(2);
            // Pega o conteúdo da resposta
            for (int i = 0; i < rx_length - 2; i++)
            {
                tx_buffer_resp[i] = rx_buffer[i];
            }
            // Pega o crc da resposta
            crcResp[0] = rx_buffer[rx_length - 2];
            crcResp[1] = rx_buffer[rx_length - 1];

            // Confere crc da resposta
            short meuCrcResp = calcula_CRC(&tx_buffer_resp[0], rx_length - 2);
            unsigned char *meuCrcByteResp = malloc(2);
            meuCrcByteResp[0] = meuCrcResp & 0xff;
            meuCrcByteResp[1] = (meuCrcResp >> 8) & 0xff;

            if (meuCrcByteResp[0] == crcResp[0] &&
                meuCrcByteResp[1] == crcResp[1]) // crc válido
                {
                    // Pega apenas o dado da resposta
                    unsigned char *floatByte = malloc(4);
                    floatByte[0] = tx_buffer_resp[3];
                    floatByte[1] = tx_buffer_resp[4];
                    floatByte[2] = tx_buffer_resp[5];
                    floatByte[3] = tx_buffer_resp[6];
                    
                    float temperatura = *(float *)&floatByte[0];
                    return temperatura;
                }
            else
                return -1;
        }
    }
    else
        return -1;
}

void mostraTemperaturas(int uartFilestream, struct bme280_dev *dev, int modoRef)
{
    float tempInt = getTemperaturaInterna(uartFilestream);
    float tempExt = getTemperaturaExterna(dev);
    float tempRef;

    if (modoRef == 1)
        tempRef = getTemperauraReferenciaPot(uartFilestream);
    else
        tempRef = pid_get_referencia();

    ClrLcd();
    if (tempInt == -1)
    {
        lcdLoc(LINE1);
        typeln("Erro ao ler TI");
    }
    else if (tempExt == -1)
    {
        lcdLoc(LINE1);
        typeln("Erro ao ler TE");
    }
    else if (tempRef == -1)
    {
        lcdLoc(LINE1);
        typeln("Erro ao ler TR");
    }
    else
    {
        lcdLoc(LINE1);
        if (modoRef == 1)
            typeln("Potenc. TR:");
        else if (modoRef == 2)
            typeln("Teclad. TR:");
        else if (modoRef == 3)
            typeln("Curva. TR:");
        
        typeFloat(tempRef);

        lcdLoc(LINE2);
        typeln("TI:");
        typeFloat(tempInt);
        typeln(" TE:");
        typeFloat(tempExt);
    }
}

void enviaSinalControle(int uartFilestream, int sinalControle)
{
    unsigned char tx_buffer[20];
    unsigned char *p_tx_buffer;
    p_tx_buffer = &tx_buffer[0];

    *p_tx_buffer++ = getEnderecoESP32(); // Endereço da ESP32
    *p_tx_buffer++ = 0x16; // Código
    *p_tx_buffer++ = 0xD1; // Sub-código
    // Matrícula
    *p_tx_buffer++ = 6;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 1;

    *p_tx_buffer++ = sinalControle & 0xFF;
    *p_tx_buffer++ = (sinalControle >> 8) & 0xFF;
    *p_tx_buffer++ = (sinalControle >> 16) & 0xFF;
    *p_tx_buffer++ = (sinalControle >> 24) & 0xFF;
    
    //crc
    short crc = calcula_CRC(&tx_buffer[0], 11);
    unsigned char *crcByte = malloc(2);
    crcByte[0] = crc & 0xff;
    crcByte[1] = (crc >> 8) & 0xff;
    *p_tx_buffer++ = crcByte[0];
    *p_tx_buffer++ = crcByte[1];

    // Envia para a UART
    int resultado = escreveUART(uartFilestream, &tx_buffer[0], p_tx_buffer);
}

void enviaSinalReferencia(int uartFilestream, float sinalReferencia)
{
    char a[sizeof(float)];
    memcpy(a, &sinalReferencia, sizeof(float));

    unsigned char tx_buffer[20];
    unsigned char *p_tx_buffer;
    p_tx_buffer = &tx_buffer[0];

    *p_tx_buffer++ = getEnderecoESP32(); // Endereço da ESP32
    *p_tx_buffer++ = 0x16; // Código
    *p_tx_buffer++ = 0xD2; // Sub-código
    // Matrícula
    *p_tx_buffer++ = 6;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 9;
    *p_tx_buffer++ = 1;

    *p_tx_buffer++ = a[0];
    *p_tx_buffer++ = a[1];
    *p_tx_buffer++ = a[2];
    *p_tx_buffer++ = a[3];
    
    //crc
    short crc = calcula_CRC(&tx_buffer[0], 11);
    unsigned char *crcByte = malloc(2);
    crcByte[0] = crc & 0xff;
    crcByte[1] = (crc >> 8) & 0xff;
    *p_tx_buffer++ = crcByte[0];
    *p_tx_buffer++ = crcByte[1];

    // Envia para a UART
    int resultado = escreveUART(uartFilestream, &tx_buffer[0], p_tx_buffer);
}

const char* getfield(char* linha, int num)
{
    const char* tok;
    for (tok = strtok(linha, ",");
            tok && *tok;
            tok = strtok(NULL, ",\n"))
    {
        if (!--num)
            return tok;
    }
    return NULL;
}

void desligaSistema()
{
    enviaEstadoSistema(uartFilestream, 0);
    desligaResistor();
    desligaVentoinha();
    close(uartFilestream);
    printf("Fim do programa\n");
    exit(0);
}

void insereLog(int uartFilestream, struct bme280_dev *dev, int modoRef)
{
    FILE* stream = fopen("log.csv", "a+");

    if (!stream)
    {
        //Erro
        printf("Não foi  possível abrir o arquivo de log\n");
        desligaSistema();
    }

    time_t rawtime;
    struct tm * timeinfo;

    time (&rawtime);
    timeinfo = localtime(&rawtime);
    char* dataHora = asctime(timeinfo);
    dataHora[strlen(dataHora) - 1] = 0;

    float tempInt = getTemperaturaInterna(uartFilestream);
    float tempExt = getTemperaturaExterna(dev);
    float tempUser;
    if (modoRef == 1)
        tempUser = getTemperauraReferenciaPot(uartFilestream);
    else
        tempUser = pid_get_referencia();

    float sinalControle = pid_get_sinal_controle();
    float valResist;
    float valVent;

    if (sinalControle > 0)
    {
        valResist = sinalControle;
        valVent = 0;
    }
    else
    {
        valResist = 0;
        valVent = sinalControle * -1;
    }

    fprintf(stream, "%s, %f, %f, %f, %f, %f\n", dataHora, tempInt, tempExt,
            tempUser, valResist, valVent);

    fclose(stream);
}
/// Meus métodos