#ifndef PID_H_
#define PID_H_

void pid_configura_constantes(double Kp_, double Ki_, double Kd_);
void pid_atualiza_referencia(float referencia_);
float pid_get_referencia();
float pid_get_sinal_controle();
double pid_controle(double saida_medida);

#endif /* PID_H_ */